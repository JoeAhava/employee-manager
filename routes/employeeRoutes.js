import express from "express";
import {
	registerEmployee,
	getEmployee,
	getEmployees,
	updateEmployee,
	deleteEmployee,
} from "../controllers/EmployeeController.js";
import { validateRegister } from "../middlewares/validateEmployee.js";
const router = express.Router();

router.route("/").get(getEmployees).post(validateRegister, registerEmployee);
router
	.route("/:id")
	.get(getEmployee)
	.put(updateEmployee)
	.delete(deleteEmployee);

export default router;
