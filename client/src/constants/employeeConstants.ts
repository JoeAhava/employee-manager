export enum EmployeeConstant {
	RESET_STATE = "RESET_STATE",
	FETCH_EMPLOYEES = "FETCH_EMPLOYEES",
	UPDATE_EMPLOYEE = "UPDATE_EMPLOYEE",
	CREATE_EMPLOYEE = "CREATE_EMPLOYEE",
	CREATE_EMPLOYEE_LOCAL = "CREATE_EMPLOYEE_LOCAL",
	DELETE_EMPLOYEE = "DELETE_EMPLOYEE",
	FETCH_EMPLOYEES_SUCCESS = "FETCH_EMPLOYEES_SUCCESS",
	UPDATE_EMPLOYEE_SUCCESS = "UPDATE_EMPLOYEE_SUCCESS",
	UPDATE_EMPLOYEE_LOCAL = "SET_EMPLOYEE_LOCAL",
	CREATE_EMPLOYEE_SUCCESS = "CREATE_EMPLOYEE_SUCCESS",
	DELETE_EMPLOYEE_SUCCESS = "DELETE_EMPLOYEE_SUCCESS",
}
