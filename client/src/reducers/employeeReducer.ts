import { EmployeeConstant } from "../constants/employeeConstants";
import { Employee } from "../models/Employee";

export const fetchEmployees = () => ({
	type: EmployeeConstant.FETCH_EMPLOYEES,
});

export const setEmployees = (data: []) => ({
	type: EmployeeConstant.FETCH_EMPLOYEES_SUCCESS,
	payload: data,
});

export const setEmployee = (data: Employee) => ({
	type: EmployeeConstant.UPDATE_EMPLOYEE_LOCAL,
	payload: data,
});

export const deleteEmployee = (data: Employee) => ({
	type: EmployeeConstant.DELETE_EMPLOYEE,
	payload: data._id,
});

export const resetState = () => ({
	type: EmployeeConstant.RESET_STATE,
});

export const employeeReducer = (
	state = {
		employees: [],
		employee: {
			id: undefined,
			name: undefined,
			salary: undefined,
			birthdate: undefined,
			gender: undefined,
		},
	},
	action: { type: EmployeeConstant; payload: any },
) => {
	const {
		loading,
		success,
		created,
		updated,
		deleted,
		error,
		...cleanState
	}: any = state;
	switch (action.type) {
		case EmployeeConstant.FETCH_EMPLOYEES:
			return { ...cleanState, loading: true };
		case EmployeeConstant.FETCH_EMPLOYEES_SUCCESS:
			return { ...cleanState, employees: action.payload, success: true };
		case EmployeeConstant.CREATE_EMPLOYEE:
			return { ...cleanState, loading: true };
		case EmployeeConstant.CREATE_EMPLOYEE_LOCAL:
			return { ...cleanState, employee: action.payload };
		case EmployeeConstant.CREATE_EMPLOYEE_SUCCESS:
			return { ...cleanState, created: true };
		case EmployeeConstant.UPDATE_EMPLOYEE:
			return { ...cleanState, loading: true };
		case EmployeeConstant.UPDATE_EMPLOYEE_SUCCESS:
			return { ...cleanState, updated: true };
		case EmployeeConstant.UPDATE_EMPLOYEE_LOCAL:
			return { ...cleanState, employee: action.payload };
		case EmployeeConstant.DELETE_EMPLOYEE:
			return { ...cleanState, loading: true };
		case EmployeeConstant.DELETE_EMPLOYEE_SUCCESS:
			return { ...cleanState, deleted: true };
		case EmployeeConstant.RESET_STATE:
			return {
				...cleanState,
				employee: {
					id: undefined,
					name: undefined,
					salary: undefined,
					birthdate: undefined,
					gender: undefined,
				},
			};
		default:
			return cleanState;
	}
};
