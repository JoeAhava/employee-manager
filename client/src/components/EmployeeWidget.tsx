import React from 'react'
import styled from 'styled-components'
import { ActionsWidget } from './ActionsWidget';
const StyledLabel = styled.td`
    padding: 1em;
    color: palevioletred;
`;

const StyledRow = styled.tr`
    border: 0.1em solid rgba(0,0,0,0.05);
    border-left: none;
    border-right: none;
    border-radius: 0.5em;
    box-shadow: 0 3px 4px 0 rgba(0,0,0,0.03);
    &:nth-child(even) {
        background-color: rgba(0,10,10,0.05);
    }
    &:hover {
        transform: scale(1.005);
    }
`;

export const EmployeeWidget = (props: any) => {
    
    return (
        <StyledRow>
            <StyledLabel>{props.employee.name}</StyledLabel>
            <StyledLabel>{props.employee.salary}</StyledLabel>
            <StyledLabel>{props.employee.gender}</StyledLabel>
            <StyledLabel>{new Date(props.employee.birthdate).toLocaleDateString()}</StyledLabel>
            <ActionsWidget onUpdate={props.onUpdate} onDelete={props.onDelete}  employee={props.employee} />
        </StyledRow>
    )
}

