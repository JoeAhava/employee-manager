import styled from "styled-components";

interface StyledModalProps {
    showModal: boolean;
}

const StyledModal = styled.div<StyledModalProps>`
    z-index: ${(props: any) => props.showModal ? "2" : "-1"};
    margin: 0 auto;
    transition: ease-in-out 0.5ms;
    display: ${(props:any) => props.showModal ? "block" : "none"};
    
`

const StyledModalHeader = styled.div`

`

const StyledModalBody = styled.div`

`

const StyledModalFooter = styled.div`

`

const Modal = (props: any) => {
    return (
        <StyledModal showModal={props.showModal}>
            <StyledModalHeader>{ props.header}</StyledModalHeader>
            <StyledModalBody>{ props.body}</StyledModalBody>
            <StyledModalFooter>{ props.footer}</StyledModalFooter>
        </StyledModal>
    )
}

export default Modal