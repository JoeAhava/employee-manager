import React from 'react'
import styled from 'styled-components'
import { FaEdit, FaTrashAlt, FaTimesCircle } from 'react-icons/fa'

// enum ButtonType {
//     DELETE,
//     EDIT
// }
export const StyledButton = styled.button`
    margin: 0.3em 0.5em;
    border: none;
    background-color: transparent;
    transition: ease-in-out 50ms;
    color: orange;
    &:hover {
        transform: scale(1.2);
    }
`

export const StyledButtonDelete = styled.button`
    margin: 0.3em 0.5em;
    border: none;
    background-color: transparent;
    transition: ease-in-out 50ms;
    color: rgba(255,0,0,0.6);
    &:hover {
        transform: scale(1.2);
    }
`

const StyledActionsTd = styled.td`

`

export const ActionsWidget = (props: any) => {
    return (
        <StyledActionsTd>
            <StyledButton onClick={() => props.onUpdate(props.employee)}>
                <FaEdit /> Edit
            </StyledButton>
            <StyledButtonDelete onClick={() => props.onDelete(props.employee)}>
                <FaTrashAlt /> Delete
            </StyledButtonDelete>
        </StyledActionsTd>
    )
}
