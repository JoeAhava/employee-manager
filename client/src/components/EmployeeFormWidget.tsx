
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import { EmployeeConstant } from '../constants/employeeConstants';
import { Employee } from '../models/Employee';
import {FaSave} from 'react-icons/fa'
const StyledForm = styled.form`
    background-color: white;
    border-radius: 0.5em;
    padding: 1em;
    display: flex;
    flex-direction: column;
    align-items: center;
`

const StyledInput = styled.input`
    padding: 0.5em;
    margin: 0.5em;
    border: 0.05em solid rgba(0, 0, 0, 0.2);
    border-radius: 0.3em;
    font-size: 1.2rem;
    &:focus {
        outline: none;
        border: 0.1em solid rgba(0, 0, 0, 0.05);
        box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.05);
        border-radius: 0.5em;
    }
`

const StyledInputDate = styled(StyledInput)`
    align-self: flex-start;
`
const StyledButton = styled.button`
    padding: 0.2em 0.5em;
	z-index: 1;
	background-color: rgba(0, 0, 0, 0.01);
	color: rgba(30, 130, 76, 1);
	border: 0.1em solid rgba(0, 0, 0, 0.05);
	border-radius: 0.2em;
	box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.1);
	&:hover {
		filter: brightness(97%);
		transform: scale(1.03);
	}
`

const StyledInputSelect = styled.select`
    padding: 0.5em;
    margin: 0.5em;
    align-self: flex-start;
    border: 0.05em solid rgba(0, 0, 0, 0.2);
    border-radius: 0.3em;
    font-size: 1.2rem;
    &:focus {
        outline: none;
        border: 0.1em solid rgba(0, 0, 0, 0.05);
        box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.05);
        border-radius: 0.5em;
    }
`

const StyledInputSelectOption = styled.option<SelectInputOptionInterface>`
    padding: 0.5em;
    margin: 0.5em;
    border: 0.05em solid rgba(0, 0, 0, 0.2);
    border-radius: 0.3em;
    font-size: 1.2rem;
    &:focus {
        outline: none;
        border: 0.1em solid rgba(0, 0, 0, 0.05);
        box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.05);
        border-radius: 0.5em;
    }
`
interface SelectInputOptionInterface {
    value: any
}

export default function EmployeeFormWidget(props: any) {
    const { employee } = useSelector((state: any) => state.employeeReducer)
    const dispatch = useDispatch()
    
    function saveEmployee(e: any) {
        e.preventDefault()
        dispatch({type: props.edit? EmployeeConstant.UPDATE_EMPLOYEE : EmployeeConstant.CREATE_EMPLOYEE, payload: employee})
    }
    const filterDate = (data: string) => {
        return (data ? (new Date(data).toISOString().substr(0, 10)) : undefined)
    }
  return (
    <StyledForm>
          <StyledInput placeholder="Name" value={employee.name} onChange={(e) => dispatch({type: EmployeeConstant.CREATE_EMPLOYEE_LOCAL, payload: {...employee, name: e.target?.value}})}></StyledInput>
          <StyledInput placeholder="Salary" type="number" step="50" value={employee.salary} onChange={(e) => dispatch({type: EmployeeConstant.CREATE_EMPLOYEE_LOCAL, payload: {...employee, salary: e.target?.value}})}></StyledInput>
          <StyledInputSelect placeholder="Select gender" value={employee.gender} onChange={(e) => dispatch({ type: EmployeeConstant.CREATE_EMPLOYEE_LOCAL, payload: { ...employee, gender: e.target?.value} })}>
              <StyledInputSelectOption value={null}>Select Gender</StyledInputSelectOption>
              <StyledInputSelectOption value='Male'>Male</StyledInputSelectOption>
              <StyledInputSelectOption value='Female'>Female</StyledInputSelectOption>
          </StyledInputSelect>
          <StyledInputDate placeholder="Birthdate" type="date" defaultValue={filterDate(employee.birthdate)} value={filterDate(employee.birthdate)} onChange={(e) => dispatch({ type: EmployeeConstant.CREATE_EMPLOYEE_LOCAL, payload: { ...employee, birthdate: e.target?.value } })}></StyledInputDate>
          <StyledButton onClick={(e) => saveEmployee(e)}><FaSave /> Save Employee </StyledButton>
    </StyledForm>
  );
}
