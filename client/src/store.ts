import { createStore, applyMiddleware, combineReducers, compose } from "redux";
import createSagaMiddleware from "@redux-saga/core";
import { employeeReducer } from "./reducers/employeeReducer";
import { watcherSaga } from "./sagas/rootSaga";

const rootReducer = combineReducers({
	employeeReducer,
});

const sagaMiddleware = createSagaMiddleware();

const middlewares = [sagaMiddleware];

const composeEnhancers =
	(window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
	rootReducer,
	composeEnhancers(applyMiddleware(...middlewares)),
);

sagaMiddleware.run(watcherSaga);

export type RootState = ReturnType<typeof store.getState>;

export default store;
