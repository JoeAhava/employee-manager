export interface Employee {
	_id: string;
	name: String;
	birthdate: Date;
	salary: Number;
	gender: String;
}
