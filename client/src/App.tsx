import { EmployeeWidget } from "./components/EmployeeWidget";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteEmployee, fetchEmployees, resetState, setEmployee } from "./reducers/employeeReducer";
import { RootState } from "./store";
import { Employee } from "./models/Employee";
import styled from "styled-components";
import Modal from "./components/ModalWidget";
import { FaSpinner, FaTrash, FaUserPlus, FaTimesCircle } from "react-icons/fa";
import EmployeeFormWidget from "./components/EmployeeFormWidget";
import { StyledButtonDelete } from "./components/ActionsWidget";

interface StyledAppProps {
	showModal: boolean;
}

interface StyledAppSurfaceProps {
	onClick: Function;
}

const StyledApp = styled.div<StyledAppProps>`
	position: relative;
	display: flex;
	flex-direction: column;
	align-items: center;
	padding: 1rem;
	margin: 0;
	background-color: ${(props: any) =>
		props.showModal ? "rgba(48, 49, 48, 0.35)" : "white"};
	min-height: 100%;
	position: fixed;
	transition: all 1.3s;
	width: 100%;
`;
const StyledAppSurface = styled.div<StyledAppSurfaceProps>`
	padding: 1rem;
	margin: 0;
	min-height: 100%;
	position: fixed;
	transition: all 1.3s;
	width: 100%;
`;
const StyledTable = styled.table`
	margin: 0.5em auto;
	min-width: 80%;
	border-collapse: collapse;
`;

const StyledCaption = styled.caption`
	margin: 0.5em auto 0 auto;
	width: 80%;
	display: flex;
	justify-content: space-between;
`;
const StyledThead = styled.thead`
	border: 0.1em solid rgba(0, 0, 0, 0.05);
	border-radius: 0.5em;
	box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.03);
	&:nth-child(even) {
		background-color: rgba(0, 0, 0, 0.1);
	}
`;

const StyledButtonCreate = styled.button`
	padding: 0.2em 0.5em;
	z-index: 1;
	background-color: rgba(0, 0, 0, 0.01);
	color: rgba(30, 130, 76, 1);
	border: 0.1em solid rgba(0, 0, 0, 0.05);
	border-radius: 0.2em;
	box-shadow: 0 2px 3px 0 rgba(0, 0, 0, 0.1);
	&:hover {
		filter: brightness(97%);
		transform: scale(1.03);
	}
`;

const StyledTbody = styled.tbody`
	border: 0.1em solid rgba(0, 0, 0, 0.05);
	border-radius: 0.1em;
	box-shadow: 0 3px 4px 0 rgba(0, 0, 0, 0.03);
	
`;

const StyledBrand = styled.h1`
	font-family: calibri;
	font-weight: 450;
	color: rgba(255, 0, 0, 0.5);
	text-align: center;
`;

const StyledContainer = styled.div`
  padding: 0;
  margin: 0 auto;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
`

const StyledTh = styled.th`
	text-align: left;
	padding: 0.5em 1em;
	color: palevioletred;
`;

const StyledMessage = styled.div<StyledMessageProps>`
	padding: 1rem;
	margin: 0 auto;
	position: absolute;
	display: ${(props: any) => props.showMessage ? "flex" : "none"};
	justify-content: center;
	align-items: center;
	color: white;
	border-radius: 0.7em;
	background-color: ${(props:any) => props.success ? "rgba(30, 130, 76, 0.8)" : "red"};
	transition: ease-in-out 1.5ms;
`
const StyledDialogueContainer = styled.div` 
	padding: 2em;
	border-radius: 0.5em;
	margin: 1em auto;
	color: red;
	background-color: whitesmoke;
`
const StyledConfirmMessage = styled.p`
	padding: 1em;
	margin: 1em;
	color: palevioletred;
`
const StyledFlex = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
`
interface StyledMessageProps {
	showMessage: boolean;
	success: boolean;
}

function App() {
	const [showModal, setShowModal] = useState(false);
	const [showMessage, setShowMessage] = useState(false);
	const [showConfirm, setShowConfirm] = useState(false)
	const [message, setMessage] = useState('')
	const [isSuccess, setIsSuccess] = useState(false)
	const [isEdit, setIsEdit] = useState(false)
	const dispatch = useDispatch();
	const { employees, success, created, updated, deleted, employee, loading }= useSelector(
		(state: any) => state.employeeReducer,
	);
	useEffect(() => {
		dispatch(fetchEmployees());
	}, [dispatch]);

	useEffect(() => {
		if (created) {
			setShowModal(false)
			setShowMessage(true)
			setIsSuccess(true)
			setMessage("Successfully created employee")
			setTimeout(() => {
				setShowMessage(false)
				setIsSuccess(false)
			}, 2000)
		}
		if (updated) {
			setShowModal(false)
			setShowMessage(true)
			setIsSuccess(true)
			setMessage("Successfully updated employee")
			setTimeout(() => {
				setShowMessage(false)
				setIsSuccess(false)
			}, 2000)
		}
		if (deleted) {
			setShowModal(false)
			setShowMessage(true)
			setIsSuccess(true)
			setMessage("Successfully deleted employee")
			setTimeout(() => {
				setShowMessage(false)
				setIsSuccess(false)
			}, 2000)
		}
		console.log('success', success)
	}, [success, created, updated, deleted]);

	useEffect(() => {
		if (!showModal && !showConfirm && !showMessage) {
			setIsEdit(false)
			dispatch(resetState())
		}
		if (showConfirm) {
			setIsEdit(false)
			setShowModal(false)
		}
	}, [showModal, showConfirm, showMessage])
	const showModalHandler = () => {
		setShowModal(true);
	};

	const closeModalHandler = () => {
		setIsEdit(false)
		setShowModal(false);
	};
	const showConfirmHandler = () => {
		setShowModal(false)
		setShowConfirm(true);
	};

	const closeConfirmHandler = () => {
		setIsEdit(false)
		setShowConfirm(false);
	};
	const editEmployeeHandler = (employee: Employee) => {
		setIsEdit(true)
		dispatch(setEmployee(employee))
		showModalHandler()
	}

	const deleteEmployeeHandler = (employee: Employee) => {
		setIsEdit(false)
		dispatch(setEmployee(employee))
		setShowConfirm(true)
	}

	const deleteEmployeeConfirm = () => {
		setShowConfirm(false)
		dispatch(deleteEmployee(employee))
	}

	return (
		<StyledApp showModal={showModal}>
			{showMessage && <StyledMessage showMessage={showMessage} success={isSuccess}>{ message }</StyledMessage>}
			{showConfirm && <StyledContainer><Modal showModal={showConfirm} body={
				<StyledDialogueContainer>
					<StyledConfirmMessage>
						Confirm delete ?
					</StyledConfirmMessage>
					<StyledFlex>
						<StyledButtonDelete onClick={() => closeConfirmHandler()}><FaTimesCircle /> Cancel</StyledButtonDelete>
						<StyledButtonDelete onClick={() => deleteEmployeeConfirm()}><FaTrash />  Delete</StyledButtonDelete>
					</StyledFlex>
				</StyledDialogueContainer>
			} /></StyledContainer>}
			{showModal && <StyledContainer><Modal showModal={showModal} body={<EmployeeFormWidget edit={isEdit} />} /></StyledContainer>}
			{showModal && (
				<StyledAppSurface onClick={closeModalHandler}></StyledAppSurface>
			)}
			<StyledBrand>Employee Manager</StyledBrand>
			{/* <button onClick={showModalHandler}>Open Modal</button> */}
			
			{loading === 0 ? (
				<FaSpinner />
			) : (
				<>
					<StyledCaption>
						<StyledButtonCreate
							onClick={showModal ? closeModalHandler : showModalHandler}
						>
							<FaUserPlus /> {showModal ? "Cancel" : "Add Employee"}
						</StyledButtonCreate>
					</StyledCaption>
					<StyledTable>
						<StyledThead>
							<tr>
								<StyledTh key="name">Name</StyledTh>
								<StyledTh key="salary">Salary</StyledTh>
								<StyledTh key="gender">Gender</StyledTh>
								<StyledTh key="birthdate">Birthdate</StyledTh>
							</tr>
						</StyledThead>
						<StyledTbody>
							{employees.map((employee: Employee) => (
								<EmployeeWidget
									key={employee._id}
									employee={employee}
									onUpdate={editEmployeeHandler}
									onDelete={deleteEmployeeHandler}
									showModalHandler={showModalHandler}
									closeModalHandler={closeModalHandler}
								/>
							))}
						</StyledTbody>
					</StyledTable>
				</>
			)}
		</StyledApp>
	);
}

export default App;
