import { AxiosResponse } from "axios";
import { put, call } from "redux-saga/effects";
import { EmployeeConstant } from "../../constants/employeeConstants";
import { fetchEmployees, setEmployees } from "../../reducers/employeeReducer";
import {
	createEmployeeRequest,
	getEmployeesRequest,
	deleteEmployeeRequest,
} from "../request/employeeRequests";

export function* handleGetUsers(action: any) {
	try {
		const response: AxiosResponse = yield call(getEmployeesRequest);
		const { data } = response;
		console.log(data);
		yield put(setEmployees(data.employees));
	} catch (e) {
		console.log(e);
	}
}

export function* handleCreateUser(action: any) {
	try {
		const response: AxiosResponse = yield call(createEmployeeRequest, {
			body: action.payload,
		});
		const { data } = response;
		console.log(data);
		yield put({
			type: EmployeeConstant.CREATE_EMPLOYEE_SUCCESS,
		});
	} catch (e) {
		console.log(e);
	}
}

export function* handleDeleteUser(action: any) {
	try {
		console.log(action.payload);
		const response: AxiosResponse = yield call(deleteEmployeeRequest, {
			body: action.payload,
		});
		const { data } = response;
		yield put({
			type: EmployeeConstant.DELETE_EMPLOYEE_SUCCESS,
		});
	} catch (e) {
		console.log(e);
	}
}

export function* handleUpdateUser(action: any) {
	try {
		const response: AxiosResponse = yield call(createEmployeeRequest, {
			body: action.payload,
		});
		const { data } = response;
		console.log(data);
		yield put({
			type: EmployeeConstant.UPDATE_EMPLOYEE_SUCCESS,
		});
	} catch (e) {
		console.log(e);
	}
}

export function* handleCreateUserSuccess() {
	try {
		yield put({
			type: EmployeeConstant.FETCH_EMPLOYEES,
		});
	} catch (e) {
		console.log(e);
	}
}
