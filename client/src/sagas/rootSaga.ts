import { takeLatest, all, fork } from "redux-saga/effects";
import { EmployeeConstant } from "../constants/employeeConstants";
import {
	handleCreateUser,
	handleGetUsers,
	handleCreateUserSuccess,
	handleUpdateUser,
	handleDeleteUser,
} from "./handler/employeesHandlers";

export function* watcherSagaFetchEmployee() {
	yield takeLatest(EmployeeConstant.FETCH_EMPLOYEES, handleGetUsers);
}

export function* watcherSagaCreateEmployee() {
	yield takeLatest(EmployeeConstant.CREATE_EMPLOYEE, handleCreateUser);
}

export function* watcherSagaUpdateEmployee() {
	yield takeLatest(EmployeeConstant.UPDATE_EMPLOYEE, handleUpdateUser);
}

export function* watcherSagaDeleteEmployee() {
	yield takeLatest(EmployeeConstant.DELETE_EMPLOYEE, handleDeleteUser);
}

export function* watcherSagaCreateEmployeeSuccess() {
	yield takeLatest(
		EmployeeConstant.CREATE_EMPLOYEE_SUCCESS,
		handleCreateUserSuccess,
	);
}

export function* watcherSagaUpdateEmployeeSuccess() {
	yield takeLatest(
		EmployeeConstant.UPDATE_EMPLOYEE_SUCCESS,
		handleCreateUserSuccess,
	);
}

export function* watcherSagaDeleteEmployeeSuccess() {
	yield takeLatest(
		EmployeeConstant.DELETE_EMPLOYEE_SUCCESS,
		handleCreateUserSuccess,
	);
}

export function* watcherSaga() {
	yield all([
		fork(watcherSagaFetchEmployee),
		fork(watcherSagaCreateEmployee),
		fork(watcherSagaCreateEmployeeSuccess),
		fork(watcherSagaUpdateEmployee),
		fork(watcherSagaUpdateEmployeeSuccess),
		fork(watcherSagaDeleteEmployee),
		fork(watcherSagaDeleteEmployeeSuccess),
	]);
}
