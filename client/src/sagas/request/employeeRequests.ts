import axios, { AxiosResponse } from "axios";
import { Employee } from "../../models/Employee";

export function getEmployeesRequest(): Promise<AxiosResponse<any>> {
	return axios.request({
		method: "GET",
		url: "/api/employees",
	});
}

export function createEmployeeRequest(
	payload: any,
): Promise<AxiosResponse<any>> {
	return axios.request({
		method: "POST",
		url: "/api/employees",
		data: payload.body,
	});
}

export function updateEmployeeRequest(
	payload: any,
): Promise<AxiosResponse<any>> {
	return axios.request({
		method: "PUT",
		url: `/api/employees/${payload.body.id}`,
		data: payload.body,
	});
}

export function deleteEmployeeRequest(
	payload: any,
): Promise<AxiosResponse<any>> {
	console.log(payload);
	return axios.request({
		method: "DELETE",
		url: `/api/employees/${payload.body}`,
	});
}
