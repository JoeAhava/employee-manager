const validateRegister = (req, res, next) => {
	const { name, gender, salary } = req.body;
	if (typeof name === "undefined" || name === null) {
		res.status(400);
		throw new Error("Name is required");
	}

	if (typeof gender === "undefined" || gender === null) {
		res.status(400);
		throw new Error("Gender is required");
	}

	if (typeof salary === "undefined" || salary === null) {
		res.status(400);
		throw new Error("Salary is required");
	}
	next();
};

export { validateRegister };
