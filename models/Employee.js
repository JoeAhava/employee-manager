import mongoose from "mongoose";

const EmployeeSchema = mongoose.Schema({
	name: {
		type: mongoose.SchemaTypes.String,
		required: true,
	},
	birthdate: {
		type: mongoose.SchemaTypes.Date,
	},
	gender: {
		type: mongoose.SchemaTypes.String,
		required: true,
	},
	salary: {
		type: mongoose.SchemaTypes.Number,
		required: true,
	},
});

const Employee = mongoose.model("Employee", EmployeeSchema);
export default Employee;
