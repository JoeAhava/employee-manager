import express from "express";
import cors from "cors";
import connectDB from "./db.config.js";
import employeeRoutes from "./routes/employeeRoutes.js";
import dotenv from "dotenv";
import path from "path";
import { notFound, errorHandler } from "./middlewares/errorHandler.js";
dotenv.config();
connectDB();

const app = express();
const PORT = process.env.PORT || 5000;
app.use(cors());
app.use(express.json());

/**
 * @description Employee routes
 */
app.use("/api/employees", employeeRoutes);

if (process.env.NODE_ENV === "production") {
	app.use(express.static(path.join("./", "/client/build")));

	app.get("*", (req, res) =>
		res.sendFile(path.resolve("./", "client", "build", "index.html")),
	);
} else {
	app.get("/", (req, res) => {
		res.send("API is running....");
	});
}

app.use(notFound);
app.use(errorHandler);
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
