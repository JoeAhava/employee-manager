import mongoose from "mongoose";

const connectDB = () =>
	mongoose.connect(
		process.env.MONGO_URI ||
			"mongodb://localhost:27017/employees?readPreference=primary&ssl=false",
		{},
		(err) =>
			err ? console.error(err) : console.log(`Mongo Connection Success`),
	);

export default connectDB;
