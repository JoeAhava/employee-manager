import asyncHandler from "express-async-handler";
import Employee from "../models/Employee.js";

const registerEmployee = asyncHandler(async (req, res) => {
	const { name, birthdate, salary, gender } = req.body;

	const employee = new Employee({
		name,
		birthdate,
		salary,
		gender,
	});

	const createdEmployee = await employee.save();
	res.json(createdEmployee);
});

const getEmployee = asyncHandler(async (req, res) => {
	const employee = await Employee.findById(req.params.id);

	if (!employee) {
		res.status(404);
		throw new Error(`Employee ${req.params.id} Not Found`);
	}

	res.json(employee);
});

const getEmployees = asyncHandler(async (req, res) => {
	const limit = req.query.limit || 10;
	const skipIndex = req.query.page || 1;
	const employees = await Employee.find()
		.limit(limit)
		.skip(limit * (skipIndex - 1));

	const count = await Employee.countDocuments();
	if (!employees) {
		res.status(404);
		throw new Error(`Employee ${req.params.id} Not Found`);
	}

	res.json({ employees, pages: Math.ceil(count / limit) });
});

const updateEmployee = asyncHandler(async (req, res) => {
	const { name, salary, gender, birthdate } = req.body;
	const employee = await Employee.findById(req.params.id);

	if (!employee) {
		res.status(404);
		throw new Error(`Employee ${req.params.id} Not Found`);
	}

	employee.name = name || employee.name;
	employee.gender = gender || employee.gender;
	employee.salary = salary || employee.salary;
	employee.birthdate = birthdate || employee.birthdate;

	const updatedEmployee = await employee.save();
	res.json(updatedEmployee);
});

const deleteEmployee = asyncHandler(async (req, res) => {
	const employee = await Employee.findById(req.params.id);

	if (!employee) {
		res.status(404);
		throw new Error(`Employee ${req.params.id} Not Found`);
	}
	await employee.remove();
	res.json({ message: "Employee removed" });
});

export {
	registerEmployee,
	getEmployee,
	getEmployees,
	updateEmployee,
	deleteEmployee,
};
